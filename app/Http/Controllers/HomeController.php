<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Notify;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user   = User::find(1);
        $notify = Notify::create([
            'user_id'   => $user->id,
            'title'     => 'title',
            'message'   => 'message',
        ]);

        $notifyAll = Notify::all();

        event(new \App\Events\LvNotifyEvent($notify));
        // broadcast(new \App\Events\LvNotifyEvent($notify));
        // broadcast(new NewComment($comment))->toOthers();
        return view('home', compact('notify', 'notifyAll'));
    }
}
