<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $table = 'notify';
    
    protected $fillable = [
        'user_id', 'title', 'message', 'read'
    ];
    
    public function users(){
        return $this->hasMany(User::class);
    }
}
